package ru.tsc.kitaev.tm.api;

import ru.tsc.kitaev.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
